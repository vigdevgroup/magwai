import gulp from 'gulp'
import fileInclude from 'gulp-file-include'
// import webpHtmlNoSvg from 'gulp-webp-html-nosvg'
import versionNumber from 'gulp-version-number'
import replace from 'gulp-replace'
// import groupCssMediaQueries from 'gulp-group-css-media-queries'
// import webpcss from 'gulp-webpcss'
import autoprefixer from 'gulp-autoprefixer'
import cleanCss from 'gulp-clean-css'
import rename from 'gulp-rename'
import plumber from 'gulp-plumber'
import notify from 'gulp-notify'
import browserSync from 'browser-sync'
import dartSass from 'sass'
import gulpSass from 'gulp-sass'
import imagemin from 'gulp-imagemin'
import newer from 'gulp-newer'
import webpack from 'webpack'
import webpackStream from 'webpack-stream'
import del from 'del'
import ifPlugin from 'gulp-if'
import concat from 'gulp-concat'
import uglify from 'gulp-uglify'

const isBuild = process.argv.includes('--build')
const isDev = !process.argv.includes('--build')

const buildFolder = './dist'
const srcFolder = './src'

const path = {
    build: {
        html: `${buildFolder}/`,
        css: `${buildFolder}/assets/css/`,
        script: `${buildFolder}/assets/js/`,
        images: `${buildFolder}/assets/img/`,
        fonts: `${buildFolder}/assets/fonts/`
    },
    src: {
        html: `${srcFolder}/pages/*.html`,
        scss: `${srcFolder}/assets/scss/style.scss`,
        script: `${srcFolder}/assets/js/main.js`,
        images: `${srcFolder}/assets/img/**/*.{jpg,jpeg,png,gif,webp}`,
        svg: `${srcFolder}/assets/img/**/*.svg`,
        fonts: `${srcFolder}/assets/fonts/**/*.{woff,woff2}`
    },
    watch: {
        html: `${srcFolder}/**/*.html`,
        scss: `${srcFolder}/assets/scss/**/*.scss`,
        script: `${srcFolder}/assets/js/**/*.js`,
        images: `${srcFolder}/assets/img/**/*.{jpg,jpeg,png,svg,gif,ico,webp}`,
        fonts: `${srcFolder}/assets/fonts/**/*.{woff,woff2}`
    },
}
//
// function cssLibs() {
//     return gulp.src(
//         [
//             './node_modules/normalize.css/normalize.css',
//             './node_modules/swiper/swiper-bundle.css',
//         ],
//         {sourcemaps: isDev})
//         .pipe(plumber(
//             notify.onError({
//                 title: 'SCSS LIBS',
//                 message: 'Error: <%= error.message %>'
//             })
//         ))
//         .pipe(concat('libs.min.css'))
//         .pipe(cleanCss())
//         .pipe(gulp.dest(path.build.css))
// }

// function jsLibs() {
//     return gulp.src(
//         [
//             './node_modules/jquery/dist/jquery.min.js',
//             './node_modules/jquery-mask-plugin/dist/jquery.mask.min.js',
//             './node_modules/swiper/swiper-bundle.min.js',
//         ],
//         {sourcemaps: isDev})
//         .pipe(plumber(
//             notify.onError({
//                 title: 'JS LIBS',
//                 message: 'Error: <%= error.message %>'
//             })
//         ))
//         .pipe(concat('libs.min.js'))
//         .pipe(uglify())
//         .pipe(gulp.dest(path.build.script))
// }

function html() {
    return gulp.src(path.src.html)
        .pipe(plumber(
            notify.onError({
                title: 'HTML',
                message: 'Error: <%= error.message %>'
            })
        ))
        .pipe(fileInclude())
        .pipe(replace(/(\.\.\/)+assets\//g, './assets/'))
        .pipe(
            ifPlugin(
                isBuild,
                versionNumber(({
                    'value': '%DT%',
                    'append': {
                        'key': '_v',
                        'cover': 0,
                        'to': [
                            'css',
                            'js'
                        ]
                    },
                    'output': {
                        'file': 'version.json'
                    }
                }))
            )
        )
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream())
}

const sass = gulpSass(dartSass)

function scss() {
    return gulp.src(path.src.scss, {sourcemaps: isDev})
        .pipe(plumber(
            notify.onError({
                title: 'SCSS',
                message: 'Error: <%= error.message %>'
            })
        ))
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: ['./node_modules']
        }))
        .pipe(replace(/\.\.\/(\.\.\/)+img\//g, '../img/'))
        .pipe(replace(/\.\.\/(\.\.\/)+fonts\//g, '../fonts/'))
        .pipe(
            ifPlugin(
                isBuild,
                autoprefixer({
                    grid: true,
                    overrideBrowserslist: ["last 3 versions"],
                    cascade: true
                })
            )
        )
        .pipe(ifPlugin(
            isBuild,
            cleanCss()
        ))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream())
}

function images() {
    return gulp.src(path.src.images)
        .pipe(plumber(
            notify.onError({
                title: 'IMAGES',
                message: 'Error: <%= error.message %>'
            })
        ))
        .pipe(newer(path.build.images))
        .pipe(ifPlugin(
            isBuild,
            gulp.dest(path.build.images)
        ))
        .pipe(
            ifPlugin(
                isBuild,
                gulp.src(path.src.images)
            )
        )
        .pipe(
            ifPlugin(
                isBuild,
                newer(path.build.images)
            )
        )
        .pipe(
            ifPlugin(
                isBuild,
                imagemin({
                    progressive: true,
                    svgoPlugins: [{removeViewBox: false}],
                    interlaced: true,
                    optimizationLevel: 3
                })
            )
        )
        .pipe(gulp.dest(path.build.images))
        .pipe(gulp.src(path.src.svg))
        .pipe(gulp.dest(path.build.images))
        .pipe(browserSync.stream())
}

function script() {
    return gulp.src(path.src.script, {sourcemaps: true})
        .pipe(plumber(
            notify.onError({
                title: 'JS',
                message: 'Error: <%= error.message %>'
            })
        ))
        .pipe(webpackStream({
            mode: isBuild ? 'production' : 'development',
            output: {
                filename: `[name].min.js`,
            },
            // plugins: [
            //     new webpack.ProvidePlugin({
            //         $: 'jquery',
            //         jQuery: 'jquery',
            //     }),
            // ],
        }))
        .pipe(gulp.dest(path.build.script))
        .pipe(browserSync.stream())
}

function fonts() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
}

function reset() {
    return del(buildFolder)
}

function server(done) {
    browserSync.init({
        server: {
            baseDir: path.build.html
        },
        notify: false,
        port: 3000
    })
}

function watcher() {
    gulp.watch(path.watch.html, html)
    gulp.watch(path.watch.scss, scss)
    gulp.watch(path.watch.script, script)
    gulp.watch(path.watch.images, images)
    gulp.watch(path.watch.fonts, fonts)
}

const mainTasks = gulp.parallel(html, scss, script, images, fonts)

export const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server))
export const build = gulp.series(reset, mainTasks)
export const clean = reset
