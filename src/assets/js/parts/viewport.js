export default function initViewport() {
    setViewport()
    window.addEventListener('resize', setViewport);
}

function setViewport() {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}

