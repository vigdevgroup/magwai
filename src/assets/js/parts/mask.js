import {maskList} from "./maskList.js";

export const mask = (parent) => {
    let inputs = parent.querySelectorAll("input[type='tel']");

    inputs.forEach(input => {
        input.dataset.mask = ''
        input.dataset.code = ''

        input.addEventListener('focus', addFirstSymbol)
        input.addEventListener('focusout', removeFirstSybmol)
        input.addEventListener('input', setMask);
        input.addEventListener('focus', setMask);
        input.addEventListener('blur', setMask);
    });
};

export const unmask = (parent) => {
    let inputs = parent.querySelectorAll("input[type='tel']");

    inputs.forEach(input => {
        input.removeEventListener('focus', addFirstSymbol)
        input.removeEventListener('focusout', removeFirstSybmol)
        input.removeEventListener('input', setMask);
        input.removeEventListener('focus', setMask);
        input.removeEventListener('blur', setMask);
    });
}

function addFirstSymbol(e) {
    if (!e.target.value) e.target.value = '+';
}

function removeFirstSybmol(e) {
    if (!(e.target.value.length > 1)) e.target.value = '';
}

function setMask() {
    let matrix = '+###############';
    let codeCountry = ''

    maskList.forEach(item => {
        let code = item.code.replace(/[\s#]/g, ''),
            phone = this.value.replace(/[\s#-)(]/g, '');

        if (phone.includes(code)) {
            codeCountry = code
            matrix = item.code;
        }
    });

    let i = 0,
        val = this.value.replace(/\D/g, '');

    let value = matrix.replace(/(?!\+)./g, function(a) {
        return /[#\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? '' : a;
    })

    this.value = value;
    this.dataset.mask = matrix
    this.dataset.code = codeCountry
}