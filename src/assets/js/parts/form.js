import {changeModal} from "./modal.js";

export default function initForm() {
    document.addEventListener("change", checkInput);
    document.addEventListener("submit", checkForm);
}

function checkInput(e) {
    let target = e.target;

    if (checkInputValid(target)) {
        target.classList.remove("_error");
        return true;
    } else if (checkInputEmpty(target)) {
        target.classList.remove("_error");
        return false;
    } else {
        target.classList.add("_error");
        return false;
    }
}

function checkInputValid(target) {
    let type = target.type;
    let valid = false;
    let regexp

    switch (type) {
        case "text":
            if (target.dataset.onlywords)
                regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]+$/;
            else regexp = /^.+$/;

            if (regexp.test(target.value.trim()) || target.value === '') valid = true;

            break;
        case "email":
            regexp = /^[\w-.]+@([-\w]+\.)+[-\w]+$/;

            if (regexp.test(target.value.trim()) || target.value === '') valid = true;

            break;
        case "textarea":
            if (target.value.trim().length > 5 || target.value === '') valid = true;

            break;
        case "tel":
            const mask = target.dataset.mask.trim()
            const code = target.dataset.code

            if (code && target.value.trim().length === mask.length) valid = true

            break;
    }

    return valid;
}

function checkInputEmpty(target) {
    let type = target.type;
    let empty = false;

    switch (type) {
        default:
            if (!target.value) empty = true;

            break;
    }

    return empty;
}



function checkForm(event) {
    event.preventDefault();

    let form = event.target;
    let errs = false;

    let data = new FormData(form);

    for(let pair of data.entries()) {
        let target = form[pair[0]];

        if ((checkInputEmpty(target) && target.dataset.required !== 'false') || !checkInputValid(target)) {
            errs = true;
            target.classList.add("_error");
        } else {
            target.classList.remove("_error");
        }
    }

    if (errs) return;

    const btn = form.querySelector('button');
    btn.disabled = true;

    // Отправка...

    btn.disabled = false;
    clearForm(form, data);
    changeModal('modal-thanks')
}

function clearForm(form, data) {
    for(let pair of data.entries()) {
        const target = form[pair[0]];
        let type = target.type;

        if (["text", "email", "textarea", "tel"].includes(type)) {
            target.value = "";
        }
    }
}
