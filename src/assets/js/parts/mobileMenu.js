let mobileBtn
let navbar
let animMobileMenu = false

export default function initMobileMenu() {
    mobileBtn = document.querySelector('.header__mobile-btn')
    navbar = document.querySelector('.header__navbar')
    mobileBtn.addEventListener('click', mobileMenuHandler)
}

function mobileMenuHandler() {
    if (!navbar.classList.contains('_showed')) openMobileMenu()
}

function openMobileMenu() {
    if (animMobileMenu) return


    navbar.classList.add('_showed')
    document.body.classList.add('_no-scroll-menu')
    animMobileMenu = true

    setTimeout(() => {
        navbar.classList.add('_show')

        document.addEventListener('click', closeMobileMenu)
        document.addEventListener('keyup', closeMobileMenu)
    }, 0)

    setTimeout(() => {
        animMobileMenu = false
    }, 310)
}

function closeMobileMenu(e) {
    if (animMobileMenu) return

    let condition = false

    if (!e.target.closest('.header__navbar')) condition = true
    if (!condition && e.code === 'Escape') condition = true
    if (!condition && e.target.closest('.btn-modal')) condition = true

    if (!condition) return

    navbar.classList.remove('_show')
    document.body.classList.remove('_no-scroll-menu')
    animMobileMenu = true

    document.removeEventListener('click', closeMobileMenu)
    document.removeEventListener('keypress', closeMobileMenu)

    setTimeout(() => {
        navbar.classList.remove('_showed')
        animMobileMenu = false
    }, 310)
}
