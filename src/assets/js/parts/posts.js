const lim = 5
let page = 1
let loading = false

let postsGrid
let postsBtn
let postsBtnWrap

export default async function initPosts() {
    postsGrid = document.querySelector('.products-grid__inner')
    postsBtn = document.querySelector('.products-grid__btn')
    postsBtnWrap = document.querySelector('.products-grid__btn-wrap')

    await getPosts()
    postsBtnWrap.classList.remove('_hidden')

    postsBtn.addEventListener('click', postsHandler)
}

async function postsHandler() {
    if (page === 7 || loading) return

    await getPosts()

    if (page === 7) postsBtnWrap.classList.add('_hidden')
}

async function getPosts() {
    const posts = await fetchPosts()

    if (posts.length) renderPosts(posts)
}

async function fetchPosts() {
    loading = true
    postsBtn.disabled = true
    let posts = []
    let resp

    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${lim}`)
        resp = await response.json()
        page++
    } catch (e) {
        console.log(e)
    } finally {
        loading = false
        postsBtn.disabled = false
    }

    if (resp) posts = resp

    return posts
}

function renderPosts(posts) {
    posts.forEach((p) => {
        const div = document.createElement('div')
        div.innerHTML = `
        <div class="product-card products-grid__item">
            <div class="product-card__img">
                <img src="./assets/img/content/product.jpg" alt="">
            </div>
            <div class="product-card__content">
                <p class="product-card__category">
                    bridge
                </p>
                <h3 class="product-card__title">
                    ${p.title}
                </h3>
                <p class="product-card__text">
                    ${p.body}
                </p>
                <div class="product-card__foot">
                    <p class="product-card__posted">
                        Posted by <a href="#">Eugenia</a>, on July 24, 2019
                    </p>
                    <a href="#" class="btn _normal _black product-card__btn">
                        Continue reading
                    </a>
                </div>
            </div>
        </div>
        `

        postsGrid.append(div.firstElementChild)
    })
}