import {mask, unmask} from "./mask.js";

let modal
let modalBody
let animModal = false

export default function initModal() {
    modal = document.querySelector('.modal')
    modalBody = modal.querySelector('.modal__body')

    document.addEventListener('click', modalHandler)
}

function modalHandler(e) {
    const target = e.target.closest('.btn-modal')

    if (!target) return

    const module = target.dataset.target
    openModal(module)
}

function openModal(module) {
    if (animModal) return

    const moduleEl = document.querySelector(`.module.${module}`)
    if (!moduleEl) return

    modalBody.innerHTML = moduleEl.innerHTML

    modal.classList.add('_showed')
    document.body.classList.add('_no-scroll')
    animModal = true

    setTimeout(() => {
        mask(modal)

        modal.classList.add('_show')

        document.addEventListener("click", closeModal);
        document.addEventListener("keyup", closeModal);
    }, 0);

    setTimeout(() => {
        animModal = false
    }, 310)
}

export function changeModal(module) {
    const moduleEl = document.querySelector(`.module.${module}`)

    modalBody.classList.add('_hide')
    animModal = true

    setTimeout(() => {
        unmask(modal)

        modalBody.innerHTML = moduleEl.innerHTML
        modalBody.classList.remove('_hide')

        animModal = false
    }, 210)
}

function closeModal(e) {
    if (animModal) return

    let condition = false

    if (e.target.classList.contains('modal')) condition = true
    if (!condition && e.target.closest('.modal__close')) condition = true
    if (!condition && e.code === 'Escape') condition = true

    if (!condition) return

    modal.classList.remove('_show')
    document.body.classList.remove('_no-scroll')
    animModal = true

    document.removeEventListener("click", closeModal);
    document.removeEventListener("keyup", closeModal);

    setTimeout(() => {
        unmask(modal)

        modal.classList.remove('_showed')
        animModal = false
    }, 310)
}

