import initForm from "./parts/form.js";
import initMobileMenu from "./parts/mobileMenu.js";
import initModal from "./parts/modal.js";
import initPosts from "./parts/posts.js";
import initViewport from "./parts/viewport.js";



window.addEventListener('load', function() {
    initViewport()
    initForm()
    initMobileMenu()
    initPosts()
    initModal()
})

